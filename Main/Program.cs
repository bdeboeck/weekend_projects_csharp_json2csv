﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            string jsonPath = @"data\sample.json";
            string json = File.ReadAllText(jsonPath);
            XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(json, "digipolis");

            XmlNodeList list = doc.SelectNodes(@"digipolis/rows");
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"data\sample.csv"))
            {
                //write the header of the CSV
                foreach (XmlNode firstLine in list[0].ChildNodes)
                {
                    file.Write(firstLine.Name + " ");
                }
                file.WriteLine();
                //write the content of the CSV
                foreach (XmlNode node in list)
                {
                    foreach (XmlNode line in node.ChildNodes)
                    {
                        file.Write(line.InnerText + " ");
                    }
                    file.WriteLine();
                }
            }

            Console.WriteLine("enter to close");
            Console.ReadLine();
        }
    }
}
